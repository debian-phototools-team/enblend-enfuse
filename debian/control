Source: enblend-enfuse
Section: graphics
Priority: optional
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders:
 Sebastian Harl <tokkee@debian.org>,
 Andreas Metzler <ametzler@debian.org>
Build-Depends:
 cm-super,
 debhelper-compat (= 13),
 gnuplot,
 graphviz,
 help2man,
 hevea,
 imagemagick,
 libboost-dev,
 libgsl-dev,
 libjpeg-dev,
 liblcms2-dev,
 libopenexr-dev,
 libpng-dev,
 libreadonly-perl,
 librsvg2-bin,
 libtiff-dev,
 libvigraimpex-dev,
 m4,
 perl,
 pkg-config,
 texlive-extra-utils,
 texlive-font-utils,
 texlive-fonts-extra,
 texlive-latex-base,
 texlive-latex-extra,
 texlive-latex-recommended,
 zlib1g-dev
# texlive-latex-base for pdflatex,
# texlive-latex-extra for bold-extra, enumitem, hyphenat, shorttoc, suffix,
#     xstring
# texlive-latex-recommended for footnote, index, listings, microtype, ragged2e
# texlive-extra-utils for texloganalyser
# texlive-font-utils for repstopdf
# texlive-fonts-extra for cmbcsc10 - #884230
# cm-super for fontenc - #980666:
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: http://enblend.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian-phototools-team/enblend-enfuse.git
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/enblend-enfuse

Package: enblend
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: hugin
Enhances: hugin
Description: image blending tool
 Enblend is a tool for compositing images. Given a set of images that overlap
 in some irregular way, Enblend overlays them in such a way that the seam
 between the images is invisible, or at least very difficult to see. It can,
 for example, be used to blend a panorama composed of several images.
 .
 It uses a Burt & Adelson multi-resolution spline. This technique tries to
 make the seams between the input images invisible. The basic idea is that
 image features should be blended across a transition zone proportional in
 size to the spatial frequency of the features. For example, objects like
 trees and windowpanes have rapid changes in color. By blending these
 features in a narrow zone, you will not be able to see the seam because the
 eye already expects to see color changes at the edge of these features.
 Clouds and sky are the opposite. These features have to be blended across a
 wide transition zone because any sudden change in color will be immediately
 noticeable.
 .
 Enblend does not align images for you. Use a tool like Hugin or PanoTools to
 do this. The TIFFs produced by these programs are exactly what Enblend is
 designed to work with.

Package: enfuse
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: hugin
Enhances: hugin
Description: image exposure blending tool
 Enfuse blends differently exposed images of the same scene into a nice output
 image, without producing intermediate HDR images that are then tonemapped to a
 viewable image. This simplified process often works much better and quicker
 than the currently known tonemapping algorithms.
 .
 The exposure blending is done using the Mertens-Kautz-Van Reeth exposure
 fusion algorithm. The basic idea is that pixels in the input images are
 weighted according to qualities such as proper exposure, good contrast, and
 high saturation. These weights determine how much a given pixel will
 contribute to the final image.
 .
 Enfuse does not align images for you. Use a tool like Hugin or PanoTools to do
 this. The TIFFs produced by these programs are exactly what Enfuse is designed
 to work with.
